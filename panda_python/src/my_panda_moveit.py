import sys
# import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
# import gcode_reader


def get_finger_state(cnt, jnt_val):
    if cnt == 3:
        jnt_val[7] = 0.001
        jnt_val[8] = 0.001
        return [True, jnt_val]
    elif cnt == 4:
        jnt_val[7] = 0.03
        jnt_val[8] = 0.03
        return [True, jnt_val]
    else:
        jnt_val[7] = 0.03
        jnt_val[8] = 0.03
        return [False, jnt_val]


print "============ Starting setup"
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface', anonymous=True)

# create objects for using moveit_commander
robot = moveit_commander.RobotCommander()

scene = moveit_commander.PlanningSceneInterface()

group = moveit_commander.MoveGroupCommander("panda_arm_hand")
display_trajectory_publisher = rospy.Publisher(
                                    '/move_group/display_planned_path',
                                    moveit_msgs.msg.DisplayTrajectory,
                                    queue_size=20)
print "============ Waiting for RVIZ..."
rospy.sleep(10)
# print "============ Starting tutorial "
# print "============ Reference frame: %s" % group.get_planning_frame()
# print "============ End effector: %s" % group.get_end_effector_link()
# print "============ Robot Groups:"
# print robot.get_group_names()
# print "============ Printing robot state"
# print robot.get_current_state()
# print "============"
# print "============ Generating plan 1"
pose_target = geometry_msgs.msg.Pose()
group_variable_values = group.get_current_joint_values()
# print "============ Joint values: ", group_variable_values

# group_variable_values[0] = 1.0


# plan2 = group.plan()
file_joint_val = open("/home/palashtut/Desktop/example.txt", "r")
lines = file_joint_val.readlines()
joint_val_recorded = []

for line in lines:
    line = line[1:-3]
    temp = line.split(',')
    data = [0, 0, 0, 0, 0, 0, 0]
    for j in range(7):
        data[j] = float(temp[j])

    joint_val_recorded.append(data)

joint_val_sent = []
prev_val = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
for k in range(len(joint_val_recorded)):
    diff = False
    for l in range(len(joint_val_recorded[k])):
        if abs(prev_val[l] - joint_val_recorded[k][l]) > 0.1:
            diff = True
    if diff:
        prev_val = joint_val_recorded[k]
        joint_val_sent.append(joint_val_recorded[k])


count = 0
gripper_stat = False

for joint_val in joint_val_sent:
    if not gripper_stat:
        joint_val.append(0.03)
        joint_val.append(0.03)
    else:
        joint_val.append(0.001)
        joint_val.append(0.001)
    group_variable_values = joint_val
    group.set_joint_value_target(group_variable_values)
    plan = group.plan()
    group.execute(plan)
    stat = get_finger_state(count, group_variable_values)

    if stat[0]:
        rospy.sleep(10)
        # print('hi ', stat[1])
        group_variable_values = stat[1]
        group.set_joint_value_target(group_variable_values)
        plan_grip = group.plan()
        group.execute(plan_grip)
        gripper_stat = True
    rospy.sleep(10)
    count = count + 1

rospy.sleep(5)
