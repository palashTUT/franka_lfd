Human-Robot Interactive Learning documentation
============================================================

Learning from demonstration for franka robot project is composed of several ROS packages that can be found on
`GitLab <https://gitlab.com/palashTUT/franka_lfd>`_. We welcome any suggestion for improvements.

The main components of the project are :

 * ``panda_python`` which imitates the trajectory taught by a user. The codes are in python and uses packages like "moveit"

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   packages
   getting_started

Please follow installation instruction in the `franka_documentation <https://frankaemika.github.io/>`_.