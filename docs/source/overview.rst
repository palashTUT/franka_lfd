Overview
========

.. figure:: _static/ros_control_moveit.png
    :align: center
    :figclass: align-center

    Schematic overview of the components.

The above diagram represents working of a package named "killer_app" to control a robot. In the package developed the "killer_app" is "panda_python" which interfaces with "MoveIt" to control the robot.

The following use cases considered for development :

 * Record the joint values in teach mode. Currently "model_example_controller" is used to collect joint values and store it to a text file. The joint values are recorded periodically.

   .. note:: Further development will be to use "Moveit to record joint values and create a GUI to take joint values according to user input.

 * Allow the user to define jobs to be performed at each way-point.
 * Obstruction detection and new path generation. 

   .. warning::
      The obstruction detection is not enabled now, the robot stops on facing obstruction.

The main packages developed/utilised are explained next.